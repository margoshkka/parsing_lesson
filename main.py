# lxml

import lxml.html as html
from urllib.request import urlopen
import use_requests
# response.xpath('//div[contains(@class,'sidebar_right')]/li/a/@href')
# sidebar_right sidebar_content-area sticky_init
# response.xpath('//div[contains(@class,"sidebar_right")].//a/@href')


main_domain_stat = 'https://habr.com/'
page = html.parse(urlopen(main_domain_stat)).getroot()

print(page)

article_text = page.xpath(
   '//li/article[contains(@class, "post")]/h2[@class="post__title"]/a[@class="post__title_link"]/text()'
)

article_links = page.xpath(
   '//li/article[contains(@class, "post")]/h2[@class="post__title"]/a[@class="post__title_link"]/@href'
)
print(article_text)
print(article_links)






