import lxml.html as html
from urllib.request import urlopen


start_url = 'https://www.mytheresa.com/en-de/shoes.html'
page = html.parse(urlopen(start_url)).getroot()

products_links = page.xpath(
            '//ul[contains(@class, "products-grid products-grid--max-3-col")]/li[@class="item"]/a/@href'
        )
print(products_links)

for link in products_links[:5]:
   page1 = html.parse(urlopen(link)).getroot()
   images = page1.xpath(
         '//div[contains(@class, "product-image-gallery")]//img[@id]/@src'
     )
   print(images)



