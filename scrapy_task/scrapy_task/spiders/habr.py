import scrapy
from ..items import Post


class HabrSpider(scrapy.Spider):
    name = "habr"
    start_urls = ['https://habr.com/']

    def parse(self, response):
        articles_links = response.xpath(
            '//li/article[contains(@class, "post")]/h2[@class="post__title"]/a[@class="post__title_link"]/@href'
        ).extract()
        for link in articles_links:
            yield scrapy.Request(url=link,
                                 callback=self.parse_post)

    def parse_post(self,response):
        post = Post()
        post['title'] = response.xpath(
            '//article[contains(@class, "post")]//h1/span/text()'
        )

        post_body = response.xpath(
            '//article[contains(@class, "post")]//div[contains(@class, "post__body post__body_full")]'
            '//div[contains(@class, "post__text")]/.'
        )

        post['text'] = post_body[0].xpath(
            '//article[contains(@class, "post")]//div[contains(@class, "post__body post__body_full")]'
            '//div[contains(@class, "post__text")]/text() | ./p/text() | ./i/text() | ./b/text()'
        )
        post['images'] = post_body[0].xpath(
            './img/@src'
        )

        yield post
