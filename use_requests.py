# lxml


import lxml.html as html
import requests


main_domain_stat = 'https://habr.com/'
req = requests.get(main_domain_stat)
page = html.fromstring(req.content)

print(page)

article_body = page.xpath(
   '//li/article[contains(@class, "post")]/div[contains(@class, "post__body")]'
)
for post in article_body:
    print(post.xpath('.//text()'))

# article_text = page.xpath(
#    '//li/article[contains(@class, "post")]/div[contains(@class, "post__body")]/div[contains(@class, "post__text")]/p/text()'
# )
#
#
# print(article_text)



# article_links = page.xpath(
#    '//li/article[contains(@class, "post")]/h2[@class="post__title"]/a[@class="post__title_link"]/@href'
# )
# print(article_links)
